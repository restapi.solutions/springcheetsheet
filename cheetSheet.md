# Including React in your Spring Boot maven build

Create a spring boot app using start.spring.io with al lthe dependencies.

initialize the git repo and then clone the front end submodule
   
   
    git submodule add pathToRepo frontend
    git submodule init
    git submodule update
    
In your pom.xml locate or create the project > properties section and add the following properties:

    <frontend-src-dir>${project.basedir}/src/main/app</frontend-src-dir>
    <node.version>v12.3.1</node.version>
    <yarn.version>v1.16.0</yarn.version>
    <frontend-maven-plugin.version>1.7.6</frontend-maven-plugin.version>

## Add plugin to run yarn

Again, in your pom.xml locate or create the project > build > plugins section and add the following plugin definition:

<plugin>
    <groupId>com.github.eirslett</groupId>
    <artifactId>frontend-maven-plugin</artifactId>
    <version>${frontend-maven-plugin.version}</version>

    <configuration>
        <nodeVersion>${node.version}</nodeVersion>
        <yarnVersion>${yarn.version}</yarnVersion>
        <workingDirectory>${frontend-src-dir}</workingDirectory>
        <installDirectory>${project.build.directory}</installDirectory>
    </configuration>

    <executions>
        <execution>
            <id>install-frontend-tools</id>
            <goals>
                <goal>install-node-and-yarn</goal>
            </goals>
        </execution>

        <execution>
            <id>yarn-install</id>
            <goals>
                <goal>yarn</goal>
            </goals>
            <configuration>
                <arguments>install</arguments>
            </configuration>
        </execution>

        <execution>
            <id>build-frontend</id>
            <goals>
                <goal>yarn</goal>
            </goals>
            <phase>prepare-package</phase>
            <configuration>
                <arguments>build</arguments>
            </configuration>
        </execution>
    </executions>
</plugin>


Stage React build output into final location

The last pom.xml modification is to add the following plugin configuration after the previous plugin to the project > build > plugins section:

    <plugin>
        <artifactId>maven-resources-plugin</artifactId>
        <version>3.0.1</version>
        <executions>
            <execution>
                <id>position-react-build</id>
                <goals>
                    <goal>copy-resources</goal>
                </goals>
                <phase>prepare-package</phase>
                <configuration>
                    <outputDirectory>${project.build.outputDirectory}/static</outputDirectory>
                    <resources>
                        <resource>
                            <directory>${frontend-src-dir}/build</directory>
                            <filtering>false</filtering>
                        </resource>
                    </resources>
                </configuration>
            </execution>
        </executions>
    </plugin>

## Run the build

The react app is in static/

mvn package and run the jar file
